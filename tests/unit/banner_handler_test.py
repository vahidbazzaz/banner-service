import json
import datetime , math, random
import pytest

from banner_app import app


@pytest.fixture()
def apigw_event():
    """ Generates API GW Event"""

    return {
        "body": '{ "test": "body"}',
        "resource": "/{proxy+}",
        "requestContext": {
            "resourceId": "123456",
            "apiId": "1234567890",
            "resourcePath": "/{proxy+}",
            "httpMethod": "POST",
            "requestId": "c6af9ac6-7b61-11e6-9a41-93e8deadbeef",
            "accountId": "123456789012",
            "identity": {
                "apiKey": "",
                "userArn": "",
                "cognitoAuthenticationType": "",
                "caller": "",
                "userAgent": "Custom User Agent String",
                "user": "",
                "cognitoIdentityPoolId": "",
                "cognitoIdentityId": "",
                "cognitoAuthenticationProvider": "",
                "sourceIp": "127.0.0.1",
                "accountId": "",
            },
            "stage": "prod",
        },
        "queryStringParameters": {"foo": "bar"},
        "headers": {
            "Via": "1.1 08f323deadbeefa7af34d5feb414ce27.cloudfront.net (CloudFront)",
            "Accept-Language": "en-US,en;q=0.8",
            "CloudFront-Is-Desktop-Viewer": "true",
            "CloudFront-Is-SmartTV-Viewer": "false",
            "CloudFront-Is-Mobile-Viewer": "false",
            "X-Forwarded-For": "127.0.0.1, 127.0.0.2",
            "CloudFront-Viewer-Country": "US",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Upgrade-Insecure-Requests": "1",
            "X-Forwarded-Port": "443",
            "Host": "1234567890.execute-api.us-east-1.amazonaws.com",
            "X-Forwarded-Proto": "https",
            "X-Amz-Cf-Id": "aaaaaaaaaae3VYQb9jd-nvCd-de396Uhbp027Y2JvkCPNLmGJHqlaA==",
            "CloudFront-Is-Tablet-Viewer": "false",
            "Cache-Control": "max-age=0",
            "User-Agent": "Custom User Agent String",
            "CloudFront-Forwarded-Proto": "https",
            "Accept-Encoding": "gzip, deflate, sdch",
        },
        "pathParameters": {"proxy": "/examplepath"},
        "httpMethod": "POST",
        "stageVariables": {"baz": "qux"},
        "path": "/examplepath",
    }


def test_get_timeslot():
    time = datetime.datetime.now()
    current_timeslot = math.floor(time.minute / 15) + 1
    assert (app.get_timeslot() == current_timeslot)

def test_get_banner_returns_correct_banner():
    source_ip = '127.0.0.1'
    campaign_id = random.choice(range(1,50))
    first_banner = app.get_banner(source_ip,campaign_id)
    second_banner = app.get_banner(source_ip,campaign_id)
    third_banner = app.get_banner(source_ip,campaign_id)

    assert type(first_banner) == int
    assert type(second_banner) == int
    assert type(third_banner) == int
    assert first_banner != second_banner != third_banner

def test_get_banners_apply_busines_rules_returns_correct_result():

    time_slot = 3
    campaign_id = 4

    mock_last_seen = [257, 258, 130, 263, 138, 268, 271, 144, 147]

    res_x = app.get_banners_by_revenue(mock_last_seen, time_slot, campaign_id)

    assert type(res_x) == list
    assert any(item in mock_last_seen for item in res_x) == False

    res_x = app.apply_business_rules(res_x, mock_last_seen + res_x, time_slot, campaign_id)

    assert type(res_x) == list
    assert any(item in mock_last_seen for item in res_x) == False

    less_than_5_mock_banners = [282, 153, 285]

    res_x = app.apply_business_rules(less_than_5_mock_banners, mock_last_seen, time_slot, campaign_id)

    assert type(res_x) == list
    assert any(item in mock_last_seen for item in res_x) == False

    x_zero_mock_banners = []

    res_x = app.apply_business_rules(x_zero_mock_banners, mock_last_seen, time_slot, campaign_id)

    assert type(res_x) == list
    assert any(item in mock_last_seen for item in res_x) == False



