# Banner Service

This is a simple implementation of the banner service using AWS

### Diagram showing the current deployment (Amazon EC2)

![Current Deployment](banner-service-ec2.png)


### The future deployment diagram (Serverless)

![Future Deployment](AWS-banner-service.png)


## Docker Container Installation & Running 


Building the container with the following command:

```bash
docker compose build
```

By using the below command, you can run the application in the background:

```bash
docker compose up -d
```

Run the following command to run and process csv files and import them into redis:

```bash
docker exec -it banner_app /bin/sh -c "python csv_import_handler.py"
```

## Manual Installtion

Running the application requires a local instance of Redis (localhost).

```bash
python3 -m venv .venv
source .env/bin/activate
pip install -r requirements.txt
```

## Running

The process begins with importing CSV data and processing banner data, as well as storing it in redis (scheduled process).

```bash

python csv_import_handler.py

```

In order to run the project, you need to run the following commands:

```bash
source .env/bin/activate
export FLASK_ENV=prodution
export FLASK_APP=banner_app/app.py
python -m flask run
```

You can visit this website by clicking here:

http://localhost:5000/campaign/1

The the selected banner will be shown

## Testing


### Unit Tests

run unit tests by running the following command: 

```bash
pytest
```

### Stress Test

stress test using K6 

```bash
k6 run script.js
```

### Stress Test Result

We currently support around 200 ops (~ 12K requests per minute) on a single node installation due to our scalable design. There will likely need to be more instances of EC2 and load blancer as well as cluster redis in order to handle more concurrent users. 


![Stress Test Result](stress-test.png)