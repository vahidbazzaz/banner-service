import http from 'k6/http';
import { sleep } from 'k6';
import { check } from "k6";

// export const options = {
//     stages: [
//       { duration: '10s', target: 100 }, // below normal load
//       { duration: '20s', target: 100 },
//       { duration: '30s', target: 100 }, // normal load
//       // { duration: '5m', target: 200 },
//       // { duration: '2m', target: 300 }, // around the breaking point
//       // { duration: '5m', target: 300 },
//       // { duration: '2m', target: 400 }, // beyond the breaking point
//       // { duration: '5m', target: 400 },
//       { duration: '10s', target: 0 }, // scale down. Recovery stage.
//     ],
//   };

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

export const options = {
  scenarios: {
    constant_request_rate: {
      executor: 'constant-arrival-rate',
      rate: 200,
      timeUnit: '1s', // 500 iterations per second, i.e. 500 RPS
      duration: '60s',
      preAllocatedVUs: 200, // how large the initial pool of VUs would be
      maxVUs: 4000, // if the preAllocatedVUs are not enough, we can initialize more
    },
  },
};


export default function () {
  // change to valid url (for production test)
  const BASE_URL = 'http://localhost:5000/campaign/';
  const res = http.get(BASE_URL + getRandomInt(1, 50));
  check(res, { 'status was 200': (r) => r.status == 200 });
  // const responses = http.batch([
  //   ['GET', BASE_URL + getRandomInt(1, 50), null, { tags: { name: 'PublicCrocs' } }],
  //   ['GET', BASE_URL + getRandomInt(1, 50), null, { tags: { name: 'PublicCrocs' } }],
  //   ['GET', BASE_URL + getRandomInt(1, 50), null, { tags: { name: 'PublicCrocs' } }],
  //   ['GET', BASE_URL + getRandomInt(1, 50), null, { tags: { name: 'PublicCrocs' } }],
  // ]);
  // check(responses, { 'status was 200': (r) => r.status == 200 });

  sleep(1);
}