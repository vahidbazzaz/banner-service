from random import random
import pandas as pd
import time
import redis
import msgpack
import json


redis_client = redis.Redis(host='redis', port=6379, db=1)


def csv_import_handler(event, context):

    s_time = time.time()
    # time taken to read data
    csv_to_redis('./csv/')
        
    e_time = time.time()
    message = "Success: Import complete! tooks ", (e_time-s_time), "seconds to finish."
    
    return {
      'statusCode': 200,
      'body': json.dumps(message)
    }


def csv_to_redis(path):
    """
    Process CSV file and calculate aggregate and store them in Redis
    """
    for i in range(1,5):
        
        # click_id,banner_id,campaign_id
        clicks = pd.read_csv(f"{path}/{i}/clicks_{i}.csv")
        # conversion_id,click_id,revenue
        conversions = pd.read_csv(f"{path}/{i}/conversions_{i}.csv")
        # banner_id,campaign_id
        impressions = pd.read_csv(f"{path}/{i}/impressions_{i}.csv")
    

        print(f'clicks_{i}:' + str(len(clicks)))
        print(f'conversions_{i}:' + str(len(conversions)))
        print(f'impressions_{i}:' + str(len(impressions)))

        # aggregating all banner with revenue and click counts 
        banner_performance = conversions.merge(clicks, how = 'inner', on = 'click_id')
        banner_performance.sort_values(by=['campaign_id','revenue'], ascending=False, inplace=True)

        banner_performance = banner_performance.groupby(['campaign_id','banner_id']).agg({'revenue': ['sum', 'count']})
        banner_performance.columns = ['revenue_sum', 'click_count']
        banner_performance = banner_performance.reset_index()
        banner_performance.sort_values(by=['campaign_id','revenue_sum','click_count'], ascending=False, inplace=True)
        banner_performance.groupby('campaign_id')

        final_banner_performance = banner_performance.groupby('campaign_id')['banner_id'].apply(list).reset_index(name="banner_id")
        # persiting list of banners with revenue in redis partioned by campaign and time slot 
        for row in final_banner_performance.itertuples(index=True, name='campaign_id'):
            redis_client.set('revenue-c'+str(row.campaign_id)+'-t'+str(i), msgpack.packb(row.banner_id))

        # aggregating all banner with click counts
        clicks_count = clicks.groupby(['campaign_id','banner_id']).agg({'click_id': ['count']})
        clicks_count.columns = ['click_count']
        clicks_count = clicks_count.reset_index()
        clicks_count.sort_values(by=['campaign_id','click_count'], ascending=False, inplace=True)
        clicks_count.groupby('campaign_id')

        final_clicks_count = clicks_count.groupby('campaign_id')['banner_id'].apply(list).reset_index(name="banner_id")
        # persiting list of banners with click count in redis partioned by campaign and time slot 
        for row in final_clicks_count.itertuples(index=True, name='campaign_id'):
            redis_client.set('click-c'+str(row.campaign_id)+'-t'+str(i), msgpack.packb(row.banner_id))
        
        # aggregating all banner with click counts
        rand_banners = impressions.groupby('campaign_id')['banner_id'].apply(list).reset_index(name="banner_id")

        for row in rand_banners.itertuples(index=True, name='campaign_id'):
            # getting the banners with click count
            c = final_clicks_count[final_clicks_count['campaign_id'] == row.campaign_id]
            # getting the banners with revenu
            b = banner_performance[banner_performance['campaign_id'] == row.campaign_id]
            # exclude the banners with click count and revenu from the list
            final = list(set(row.banner_id) - set(set(c['banner_id'].tolist()[0])) - set(set(b['banner_id'].tolist())))
            redis_client.set('rand-banner-c'+str(row.campaign_id)+'-t'+str(i), msgpack.packb(final))

if __name__ == "__main__":
    s_time = time.time()
    # time taken to read data
    csv_to_redis('csv')
        
    e_time = time.time()

    print("Success: Import complete! tooks ", (e_time-s_time), "seconds to finish.")
