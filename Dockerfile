FROM python:3.9

WORKDIR usr/src/flask_app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY ./banner_app/. .
COPY ./data/. .
COPY ./.env ./.env
COPY ./csv_import_handler.py ./csv_import_handler.py