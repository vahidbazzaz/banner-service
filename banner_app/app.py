# import required modules
from random import random
import pandas as pd
import time
import math
import datetime
import pytest
import redis
import random
import msgpack
from flask import Flask
from flask import request
from flask import render_template, abort


app = Flask(__name__, static_url_path='/static')


# def banner_handler(event, context):

#     banner_id = get_banner(request.remote_addr, campaign_id)

#     return {
#         'statusCode':  301,
#         'headers': {
#           'location': ''
#         }
#     }

# redis_host_endpoint = os.environ["REDIS_HOST_ENDPOINT"]

redis_client = redis.Redis(host= 'redis', port=6379, db=1)


@app.route('/campaign/<int(min=1, max=50):campaign_id>', methods=['GET'])
def get_campaign_id(campaign_id):
    try:
        visitor_ip = ''
        if request.headers['X-Forwarded-For'] != None:
            visitor_ip = request.headers['X-Forwarded-For']
        elif request.headers['X-Real-IP'] != None:
            visitor_ip = request.headers['X-Real-IP']
        else:
            visitor_ip = request.remote_addr

        banner_id = get_banner(visitor_ip, campaign_id)
        return render_template('banner.html', banner='images/image_' + str(banner_id) + '.png')
    except Exception as e:
        abort(404, description="Resource not found")

def get_banner(visitor_id, campaign_id) -> list:
    """
    Get banners with the given visitor unique id and campaign
    """

    # c = connect(redis_host_endpoint)
    # r_cluter_on = rediscluster.RedisCluster(connection_pool=c)

    time_slot = get_timeslot()
    
    print('campaign_id:' + str(campaign_id) + ' time_slot:'+ str(time_slot))
    print('visitor_id:' + str(visitor_id))

    exclude_banners = get_last_banners_seen(visitor_id)

    x = get_banners_by_revenue(exclude_banners, campaign_id, time_slot)
    
    # apply business rules
    final_banners = apply_business_rules(x , exclude_banners, campaign_id, time_slot)

    # select one banner in random order
    shuffled = list(final_banners)
    random.shuffle(shuffled)

    set_last_banners_seen(visitor_id, [shuffled[0]])

    return shuffled[0]

def get_banners_by_clicks(exclude_banners, campaign_id, time_slot) -> list:
    """
    Get the top banners by click count
    """
    banners = msgpack.unpackb(redis_client.get('click-c'+str(campaign_id)+'-t'+str(time_slot)))

    if exclude_banners:
        banners = list(set(banners) - set(exclude_banners))

    return banners

def get_banners_by_revenue(exclude_banners, campaign_id, time_slot) -> list:
    """
    Get the top banners by revenue
    """

    banners = msgpack.unpackb(redis_client.get('revenue-c'+str(campaign_id)+'-t'+str(time_slot)))

    if exclude_banners:
        banners = list(set(banners) - set(exclude_banners))

    return  banners[:10]

def get_random_banners(total_needed, exclude_banners, campaign_id, time_slot) -> list:
    """
    Get the random banners
    """
    banners = msgpack.unpackb(redis_client.get('rand-banner-c'+str(campaign_id)+'-t'+str(time_slot)))
    
    if exclude_banners:
        banners = list(set(banners) - set(exclude_banners))

    shuffled = list(banners[:total_needed])
    random.shuffle(shuffled)

    return shuffled

def get_timeslot():
    """
    Get current time slot - server time
    """
    timeslot = datetime.datetime.now()
    return math.floor(timeslot.minute / 15) + 1

def set_last_banners_seen(visitor_id, banners):
    redis_client.set('ip-'+str(visitor_id), msgpack.packb(banners))

def get_last_banners_seen(visitor_id):
    try:
        banners = msgpack.unpackb(redis_client.get('ip-'+str(visitor_id)))
    except TypeError:
        return []
    return banners


def apply_business_rules(x , exclude_banners, campaign_id, time_slot):
    """
    Apply the business rule to display the visitor
    """
    
    # If you assume X is the number of banners with conversions within a campaign, 
    # then there are a few possible scenarios:
    total_banners_revenue = len(x)

    if total_banners_revenue >= 10:
        # Show the Top 10 banners based on revenue within that campaign
        return x[:10]

    elif total_banners_revenue in range(5,10):
            # Show the Top x banners based on revenue within that campaign
            return x
    else:
        # Banners with the most clicks within that campaign 
        x_click = get_banners_by_clicks(x + exclude_banners, campaign_id, time_slot)  
        
        if total_banners_revenue in range(1,5):
            # Your collection of banners should consists of 5 banners, containing:
            # The top x banners based on revenue within that campaign 
            extra_banner_needed = 5 - len(x)    
            return x + x_click[:extra_banner_needed]

        elif total_banners_revenue == 0:
            # Show the top­5 banners based on clicks.
            extra_banner_needed = 5 - len(x_click) if(len(x_click) != 0) else 5
             
            # If the amount of banners with clicks are less than 5 within that campaign, 
            # then you should add random banners to make up a collection of 5 unique banners.
            x_random = get_random_banners(extra_banner_needed, exclude_banners, campaign_id, time_slot)
            if(len(x_random) == extra_banner_needed):
                return x_click + x_random
            else:
                x_random = get_random_banners(extra_banner_needed, exclude_banners, random.choice(range(1,50)), time_slot) 
                return x_click + x_random 

#if __name__ == "__main__":
    #app.run(host='0.0.0.0')

